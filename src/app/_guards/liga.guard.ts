import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class LigaGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('currentType')=='liga') {
      return true;
    }

    if (localStorage.getItem('currentType')=='admin') {
      this.router.navigate(['home/admin']);
    }

    if (localStorage.getItem('currentType')=='arbitro') {
      this.router.navigate(['home/arbitro']);
    }

    if (localStorage.getItem('currentType')=='equipo') {
      this.router.navigate(['home/equipo']);
    }
  }
}
