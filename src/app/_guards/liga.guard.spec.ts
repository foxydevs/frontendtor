import { TestBed, async, inject } from '@angular/core/testing';

import { LigaGuard } from './liga.guard';

describe('LigaGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LigaGuard]
    });
  });

  it('should ...', inject([LigaGuard], (guard: LigaGuard) => {
    expect(guard).toBeTruthy();
  }));
});
