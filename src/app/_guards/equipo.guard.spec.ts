import { TestBed, async, inject } from '@angular/core/testing';

import { EquipoGuard } from './equipo.guard';

describe('EquipoGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EquipoGuard]
    });
  });

  it('should ...', inject([EquipoGuard], (guard: EquipoGuard) => {
    expect(guard).toBeTruthy();
  }));
});
