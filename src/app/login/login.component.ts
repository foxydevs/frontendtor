import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { AuthService } from "./../_services/auth.service";

import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
auth:any
closeResult: string;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthService,
    private _service: NotificationsService) { }

    public options = {
    position: ["bottom", "right"],
    timeOut: 3000,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    lastOnBottom: false,
    preventDuplicates: true,
    animate: "scale",
    maxLength: 400
  };

  create(text) {
         this._service.error('Error!','Ha ocurrido un error.'+text)

  }
  toast(text){
    // Get the snackbar DIV
    var x = document.getElementById("toast")

        // Add the "show" class to DIV
        x.innerHTML=text;
        x.className = "show bg-danger";


        // After 3 seconds, remove the show class from DIV
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  }
  login(formValue:any){
   //console.log(`user: ${formValue.username} pass: ${formValue.password}`)

   $('#Loading').css('display','block')
   $('#Loading').addClass('in')

    this.authenticationService.Authentication(formValue)
      .then(response => {
        this.auth = response

        // console.log(response)
        let type:string = null;
        localStorage.setItem('currentUser', response.username);
        localStorage.setItem('currentEmail', response.email);
        localStorage.setItem('currentId', response.id);
        localStorage.setItem('currentPicture', response.picture);
        localStorage.setItem('currentState', response.estado);
        localStorage.setItem('currentAdmin', response.admin);



        if(response.equipo){
          type = 'equipo';
          localStorage.setItem('currentFirstName', response.equipos.nombre);
          localStorage.setItem('currentLastName', '');
          localStorage.setItem('currentLocalId', response.equipo);
        }else
        if(response.liga){
          type = 'liga';
          localStorage.setItem('currentFirstName', response.ligas.nombre);
          localStorage.setItem('currentLastName', '');
          localStorage.setItem('currentLocalId', response.liga);
        }else
        if(response.arbitro){
          type = 'arbitro';
          localStorage.setItem('currentFirstName', response.arbitros.nombre);
          localStorage.setItem('currentLastName', response.arbitros.apellido);
          localStorage.setItem('currentLocalId', response.arbitro);
        }else
        {
          type = 'admin';
          localStorage.setItem('currentFirstName', response.username);
          localStorage.setItem('currentLastName', '');
        }

        localStorage.setItem('currentType', type);
        // console.log(type)
        console.clear
        this.router.navigate([`home/${type}`])
      }).catch(error => {
        console.clear
   $('#Loading').css('display','none')

        this.create(error)
        this.toast(error)

      })


  }
  ngOnInit() {
  }

}
