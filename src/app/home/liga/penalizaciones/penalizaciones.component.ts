import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { PartidosService } from "./../../admin/_services/_asignaciones/partidos.service";
import { TipoPartidosService } from "./../../admin/_services/tipo-partidos.service";
import { TorneosService } from "./../../admin/_services/torneos.service";
import { LigasService } from "./../../admin/_services/ligas.service";
import { PenalizacionesService } from "./../_services/penalizaciones.service";
import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  selector: 'app-penalizaciones',
  templateUrl: './penalizaciones.component.html',
  styleUrls: ['./penalizaciones.component.css']
})
export class PenalizacionesComponent implements OnInit {
  title:any = "Tarjetas"
  Table:any
  goles:any
  comboParent:any
  comboSecondParent:any
  selectedData:any
  jugadoresData:any = []
  jugadoresData1:any
  firstHora:any = "20:00:00"
  beginDate:any
  diasConst:any = 7
  public rowsOnPage = 5;
  public search:any
  id=localStorage.getItem('currentLocalId');
  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private mainService: PartidosService,
    private secondParentService: TorneosService,
    private ligaService: LigasService,
    private firstService: PenalizacionesService,
    private parentService: TipoPartidosService
  ) { }

    ngOnInit() {

      this.cargarAll()
      this.cargarParentCombo()
      this.cargarSecondParentCombo()
      let date = new Date();
      let month = date.getMonth()+1;
      let month2;
      let day = date.getDate();
      let day2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month
      }
      if(day<10){
        day2='0'+day;
      }else{
        day2=day
      }
      this.beginDate= date.getFullYear()+'-'+month2+'-'+day2
      this.aumentarDate(month+'/'+day+'/'+date.getFullYear(),1)
    }
    aumentarDate(fecha:string,cant:number){
      let date = new Date(fecha);
      date.setDate(date.getDate() + cant)
      let month = date.getMonth()+1;
      let month2;
      let day = date.getDate();
      let day2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month
      }
      if(day<10){
        day2='0'+day;
      }else{
        day2=day
      }
      this.beginDate= date.getFullYear()+'-'+month2+'-'+day2
    }
    cargarAll(){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.ligaService.getAllMine(+this.id)
                        .then(response => {
                          this.Table = response
                          $("#editModal .close").click();
                          $("#insertModal .close").click();
                          $('#Loading').css('display','none')
                          console.clear
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    cargarParentCombo(){
      this.parentService.getAll()
                        .then(response => {
                          this.comboParent = response
                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    cargarSecondParentCombo(){
      this.secondParentService.getAllMine(+this.id)
                        .then(response => {
                          this.comboSecondParent = response
                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    cargarSingle(id:number){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.getSingle(id)
                        .then(response => {
                          this.selectedData = response;
                          this.cargarSecondChilds(this.selectedData.id)
                          // console.log(response);
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    cargarChilds(equipo:number){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.getChilds(equipo)
                        .then(response => {
                          this.jugadoresData1 = response;
                          this.cargarSecondChilds(this.selectedData.id)
                          // console.log(response);
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    cargarSecondChilds(equipo:number){
      this.firstService.getTarjetas(equipo)
                        .then(response => {
                          this.goles = response;
                          this.jugadoresData.length = 0
                          if(this.jugadoresData1){
                            this.jugadoresData1.forEach(element => {
                              this.goles.forEach(data => {
                                if(data.jugador==element.id){
                                  element.goles = data.cantidad
                                }
                              })
                              this.jugadoresData.push(element);
                            });
                          }
                          // console.log(this.jugadoresData);
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    added(val:any,jugador:number,equipo:number,partido:number){
      // console.log(`${val} - ${jugador} - ${equipo} - ${partido}`);
      let data = {
        cantidad: val,
        jugador: jugador,
        partido: partido,
        equipo: equipo
      }
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.firstService.create(data)
                        .then(response => {
                          console.clear
                          this.create('Goles Ingresados')
                          this.cargarSecondChilds(this.selectedData.id)
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

    }
    update(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      // console.log(formValue)
      this.mainService.update(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Partido Actualizado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

    }
    delete(id:string){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      if(confirm("¿Desea eliminar el Partido?")){
      this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Partido Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
      }else{
        $('#Loading').css('display','none')
      }

    }
    addDate(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.create(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Partido Ingresado')
                          let fech=formValue.fecha.split('-')
                          this.aumentarDate(fech[1]+'-'+fech[2]+'-'+fech[0],formValue.cant)
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

    }
    insert(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.create(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Partido Ingresado')
                          $('#Loading').css('display','none')
                          $('#insert-form')[0].reset()
                          $("#insertAutoModal .close").click();

                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }

  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }
}
