import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LigaComponent } from "./liga.component";
import { DashboardComponent } from "./dashboard/dashboard.component";

import { JugadoresComponent } from './jugadores/jugadores.component';
import { EquiposComponent } from './equipos/equipos.component';
import { TorneosComponent } from './torneos/torneos.component';
import { ArbitrosComponent } from './arbitros/arbitros.component';
import { PartidosComponent } from './partidos/partidos.component';
import { EquiposPartidosComponent } from './equipos-partidos/equipos-partidos.component';
import { JugadoresEquiposComponent } from './jugadores-equipos/jugadores-equipos.component';
import { ArbitrajeComponent } from './arbitraje/arbitraje.component';
import { CalendarioComponent } from './calendario/calendario.component';
import { PenalizacionesComponent } from './penalizaciones/penalizaciones.component';
import { GruposComponent } from './grupos/grupos.component';

import { ProfileComponent } from "./profile/profile.component";

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: LigaComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'equipos', component: EquiposComponent },
    { path: 'jugadores', component: JugadoresComponent },
    { path: 'torneos', component: TorneosComponent },
    { path: 'arbitros', component: ArbitrosComponent },
    { path: 'arbitraje', component: ArbitrajeComponent },
    { path: 'partidos', component: PartidosComponent },
    { path: 'calendario', component: CalendarioComponent },
    { path: 'penalizaciones', component: PenalizacionesComponent },
    { path: 'equipos-partidos', component: EquiposPartidosComponent },
    { path: 'jugadores-equipos', component: JugadoresEquiposComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'grupos', component: GruposComponent },
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LigaRoutingModule { }
