import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { ArbitrosService } from "./../../admin/_services/arbitros.service";
import { NotificationsService } from 'angular2-notifications';

declare var $: any
import { path } from "../../../config.module";

@Component({
  selector: 'app-arbitros',
  templateUrl: './arbitros.component.html',
  styleUrls: ['./arbitros.component.css']
})
export class ArbitrosComponent implements OnInit {
  title:any = "Arbitros"
  Table:any
  selectedData:any
  public rowsOnPage = 5;
  public search:any
  id=localStorage.getItem('currentLocalId');
  private basePath:string = path.path
  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private mainService: ArbitrosService
  ) { }

    ngOnInit() {

      this.cargarAll()
    }
    subirImagenes(archivo,form,id){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      var archivos=archivo.srcElement.files;
      let url = `${this.basePath}/api/arbitros/${form.id}/upload`

      var i=0;
      var size=archivos[i].size;
      var type=archivos[i].type;
          if(size<(2*(1024*1024))){
            if(type=="image/png" || type=="image/jpeg" || type=="image/jpg"){
          $("#"+id).upload(url,
              {
                avatar: archivos[i]
            },
            function(respuesta)
            {
              $('#imgAvatar').attr("src",'')
              $('#imgAvatar').attr("src",respuesta.picture)
              $('#Loading').css('display','none')
              $("#"+id).val('')
              $("#barra_de_progreso").val(0)
            },
            function(progreso, valor)
            {

              $("#barra_de_progreso").val(valor);
            }
          );
            }else{
              this.createError("El tipo de imagen no es valido")
              $('#Loading').css('display','none')
            }
        }else{
          this.createError("La imagen es demaciado grande")
          $('#Loading').css('display','none')
        }
    }
    cargarAll(){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.getAll()
                        .then(response => {
                          this.Table = response
                          $("#editModal .close").click();
                          $("#insertModal .close").click();
                          $('#Loading').css('display','none')
                          console.clear
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    cargarSingle(id:number){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.getSingle(id)
                        .then(response => {
                          this.selectedData = response;
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    update(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      //console.log(data)
      this.mainService.update(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Tipo de Equipos Actualizado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

    }
    delete(id:string){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      if(confirm("¿Desea eliminar el Tipo de Equipos?")){
      this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Tipo de Equipos Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
      }else{
        $('#Loading').css('display','none')
      }

    }
    insert(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      formValue.liga = +this.id;
      this.mainService.create(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Tipo de Equipos Ingresado')
                          $('#Loading').css('display','none')
                          $('#insert-form')[0].reset()

                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })


    }

  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }
}
