import { TestBed, inject } from '@angular/core/testing';

import { PenalizacionesService } from './penalizaciones.service';

describe('PenalizacionesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PenalizacionesService]
    });
  });

  it('should be created', inject([PenalizacionesService], (service: PenalizacionesService) => {
    expect(service).toBeTruthy();
  }));
});
