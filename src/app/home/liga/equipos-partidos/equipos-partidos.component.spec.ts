import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquiposPartidosComponent } from './equipos-partidos.component';

describe('EquiposPartidosComponent', () => {
  let component: EquiposPartidosComponent;
  let fixture: ComponentFixture<EquiposPartidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquiposPartidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquiposPartidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
