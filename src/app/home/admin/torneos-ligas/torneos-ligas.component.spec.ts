import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TorneosLigasComponent } from './torneos-ligas.component';

describe('TorneosLigasComponent', () => {
  let component: TorneosLigasComponent;
  let fixture: ComponentFixture<TorneosLigasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TorneosLigasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TorneosLigasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
