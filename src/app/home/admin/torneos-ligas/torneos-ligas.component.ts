import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { TorneosLigasService } from "../_services/_asignaciones/torneos-ligas.service";
import { LigasService } from "../_services/ligas.service";
import { TorneosService } from "../_services/torneos.service";
import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  selector: 'app-torneos-ligas',
  templateUrl: './torneos-ligas.component.html',
  styleUrls: ['./torneos-ligas.component.css']
})
export class TorneosLigasComponent implements OnInit {
  title:any = "Asignacion de Torneos a Ligas"
  Table:any
  selectedData:any[]
  droppedItemsId:any=[]
  childs:any[]
  childsId:any=[]
  droppedItems:any=[]
  parentCombo:any
  selectedParent:any
  public rowsOnPage = 5;
  public search:any
  public search1:any
  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private mainService: TorneosLigasService,
    private ChildsService: TorneosService,
    private ParentsService: LigasService
  ) { }
  ngOnInit() {
    this.cargarAll()
    this.cargarFree()
    this.ParentsService.getAll()
                      .then(response => {
                        this.parentCombo = response
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }
  onItemDrop(e: any) {
      // Get the dropped data here
      if(!this.selectedParent){
        this.createError("Debe seleccionar una Liga")
      }else{
        let existe=(this.selectedData.find(dat=>{
          return dat.id==e.dragData.id
        }))
        if(!existe){
          this.droppedItemsId.push({"id":e.dragData.id});
          this.selectedData.push(e.dragData);
          this.childsId.splice(this.childsId.findIndex(dat=>{
            return dat.id==e.dragData.id
          }),1)
        }
        // this.childs.splice(this.childs.findIndex(dat=>{
        //   return dat.id==e.dragData.id
        // }),1)
      }
  }

  onItemRemove(e: any) {
      // Get the dropped data here
      // let existe=(this.childs.find(dat=>{
      //   return dat.id==e.dragData.id
      // }))
      // if(!existe){
        this.childsId.push({"id":e.dragData.id});
        // this.childs.push(e.dragData);
        this.selectedData.splice(this.selectedData.findIndex(dat=>{
          return dat.id==e.dragData.id
        }),1)
        this.droppedItemsId.splice(this.droppedItemsId.findIndex(dat=>{
          return dat.id==e.dragData.id
        }),1)
      //}



  }
  limpiar(){
    if(this.selectedData){
      this.selectedData.length=0
      this.droppedItemsId.length=0
      this.droppedItems.length=0
      this.selectedParent=null
    }
  }
  cargarFree(){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.ChildsService.getAll()
                      .then(response => {
                        this.childs = response
                        // this.childs.forEach((item,index)=>{
                        //   this.childsId.push({"id":item.id});
                        // })
                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })
  }
  cargarAll(){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.ParentsService.getBussy()
                      .then(response => {
                        this.Table = response
                        $("#editModal .close").click();
                        $("#insertModal .close").click();
                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })
  }
  cargarSingle(id:number){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.selectedParent=id
    if(this.droppedItemsId.length){
      this.droppedItemsId.length = 0;
    }
    this.cargarFree()
    this.mainService.getMyChilds(id)
                      .then(response => {
                        this.selectedData = response
                        this.selectedData.forEach((item,index)=>{
                          this.droppedItemsId.push({"id":item.id});
                        })
                        $('#Loading').css('display','none')
                        console.clear

                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })
  }
  update(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    //console.log(data)
    this.mainService.update(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Ciclo Actualizado exitosamente')
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })

  }
  delete2(id:string){
    this.mainService.delete(id)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Torneo Desasignados')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })

  }
  delete(formValue){
    this.mainService.deleteAll(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Torneo Desasignados')
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })

  }
  insert(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    formValue = {
      "liga": this.selectedParent,
      "torneos": this.droppedItemsId
    }

    let formValueDel = {
      "liga":this.selectedParent,
      "torneos": this.childsId
    }

  if(this.selectedParent){
    this.mainService.create(formValue)
                      .then(response => {
                        this.create('Torneo Asignados')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })

                    }else{
                      this.createError("Debe seleccionar una Liga")
                    }
    this.delete(formValueDel)
  }

  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

  create(success) {
              this._service.success('¡Éxito!',success)

  }
  createError(error) {
              this._service.error('¡Error!',error)

  }
}
