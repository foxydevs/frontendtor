import { TestBed, inject } from '@angular/core/testing';

import { LigasService } from './ligas.service';

describe('LigasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LigasService]
    });
  });

  it('should be created', inject([LigasService], (service: LigasService) => {
    expect(service).toBeTruthy();
  }));
});
