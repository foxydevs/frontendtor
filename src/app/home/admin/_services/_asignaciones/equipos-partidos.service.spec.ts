import { TestBed, inject } from '@angular/core/testing';

import { EquiposPartidosService } from './equipos-partidos.service';

describe('EquiposPartidosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EquiposPartidosService]
    });
  });

  it('should be created', inject([EquiposPartidosService], (service: EquiposPartidosService) => {
    expect(service).toBeTruthy();
  }));
});
