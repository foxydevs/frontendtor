import { TestBed, inject } from '@angular/core/testing';

import { JugadoresEquiposService } from './jugadores-equipos.service';

describe('JugadoresEquiposService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JugadoresEquiposService]
    });
  });

  it('should be created', inject([JugadoresEquiposService], (service: JugadoresEquiposService) => {
    expect(service).toBeTruthy();
  }));
});
