import { TestBed, inject } from '@angular/core/testing';

import { ArbitrajeService } from './arbitraje.service';

describe('ArbitrajeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArbitrajeService]
    });
  });

  it('should be created', inject([ArbitrajeService], (service: ArbitrajeService) => {
    expect(service).toBeTruthy();
  }));
});
