import { TestBed, inject } from '@angular/core/testing';

import { TorneosLigasService } from './torneos-ligas.service';

describe('TorneosLigasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TorneosLigasService]
    });
  });

  it('should be created', inject([TorneosLigasService], (service: TorneosLigasService) => {
    expect(service).toBeTruthy();
  }));
});
