import { TestBed, inject } from '@angular/core/testing';

import { TipoTorneosService } from './tipo-torneos.service';

describe('TipoTorneosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoTorneosService]
    });
  });

  it('should be created', inject([TipoTorneosService], (service: TipoTorneosService) => {
    expect(service).toBeTruthy();
  }));
});
