import { TestBed, inject } from '@angular/core/testing';

import { TipoJugadoresService } from './tipo-jugadores.service';

describe('TipoJugadoresService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoJugadoresService]
    });
  });

  it('should be created', inject([TipoJugadoresService], (service: TipoJugadoresService) => {
    expect(service).toBeTruthy();
  }));
});
