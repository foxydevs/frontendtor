import { TestBed, inject } from '@angular/core/testing';

import { TipoEquiposService } from './tipo-equipos.service';

describe('TipoEquiposService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoEquiposService]
    });
  });

  it('should be created', inject([TipoEquiposService], (service: TipoEquiposService) => {
    expect(service).toBeTruthy();
  }));
});
