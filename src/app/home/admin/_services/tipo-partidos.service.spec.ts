import { TestBed, inject } from '@angular/core/testing';

import { TipoPartidosService } from './tipo-partidos.service';

describe('TipoPartidosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoPartidosService]
    });
  });

  it('should be created', inject([TipoPartidosService], (service: TipoPartidosService) => {
    expect(service).toBeTruthy();
  }));
});
