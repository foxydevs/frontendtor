import { TestBed, inject } from '@angular/core/testing';

import { ArbitrosService } from './arbitros.service';

describe('ArbitrosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArbitrosService]
    });
  });

  it('should be created', inject([ArbitrosService], (service: ArbitrosService) => {
    expect(service).toBeTruthy();
  }));
});
