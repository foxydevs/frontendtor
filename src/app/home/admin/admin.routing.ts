import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from "./admin.component";
import { DashboardComponent } from "./dashboard/dashboard.component";

import { UsuariosComponent } from "./usuarios/usuarios.component";
import { UsuariosTipoComponent } from "./usuarios-tipo/usuarios-tipo.component";

import { TipoEquiposComponent } from "./tipos/tipo-equipos/tipo-equipos.component";
import { TipoJugadoresComponent } from './tipos/tipo-jugadores/tipo-jugadores.component';
import { TipoPartidosComponent } from './tipos/tipo-partidos/tipo-partidos.component';
import { TipoTorneosComponent } from './tipos/tipo-torneos/tipo-torneos.component';
import { JugadoresComponent } from './jugadores/jugadores.component';
import { EquiposComponent } from './equipos/equipos.component';
import { TorneosComponent } from './torneos/torneos.component';
import { LigasComponent } from './ligas/ligas.component';
import { ArbitrosComponent } from './arbitros/arbitros.component';
import { TorneosLigasComponent } from './torneos-ligas/torneos-ligas.component';
import { PartidosComponent } from './partidos/partidos.component';
import { EquiposPartidosComponent } from './equipos-partidos/equipos-partidos.component';
import { JugadoresEquiposComponent } from './jugadores-equipos/jugadores-equipos.component';
import { ArbitrajeComponent } from './arbitraje/arbitraje.component';
import { GruposComponent } from './grupos/grupos.component';

import { ProfileComponent } from "./profile/profile.component";


const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: AdminComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'usuarios', component: UsuariosComponent },
    { path: 'usuarios-tipo', component: UsuariosTipoComponent },
    { path: 'tipo-equipos', component: TipoEquiposComponent },
    { path: 'tipo-jugadores', component: TipoJugadoresComponent },
    { path: 'tipo-partidos', component: TipoPartidosComponent },
    { path: 'tipo-torneos', component: TipoTorneosComponent },
    { path: 'equipos', component: EquiposComponent },
    { path: 'jugadores', component: JugadoresComponent },
    { path: 'torneos', component: TorneosComponent },
    { path: 'arbitros', component: ArbitrosComponent },
    { path: 'arbitraje', component: ArbitrajeComponent },
    { path: 'ligas', component: LigasComponent },
    { path: 'partidos', component: PartidosComponent },
    { path: 'equipos-partidos', component: EquiposPartidosComponent },
    { path: 'jugadores-equipos', component: JugadoresEquiposComponent },
    { path: 'torneos-ligas', component: TorneosLigasComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'grupos', component: GruposComponent },
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
