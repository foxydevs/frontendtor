import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { EquiposPartidosService } from "../_services/_asignaciones/equipos-partidos.service";
import { EquiposService } from "../_services/equipos.service";
import { TorneosService } from "../_services/torneos.service";
import { TipoPartidosService } from "../_services/tipo-partidos.service";
import { GruposService } from "./../_services/grupos.service";
import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  selector: 'app-equipos-partidos',
  templateUrl: './equipos-partidos.component.html',
  styleUrls: ['./equipos-partidos.component.css']
})
export class EquiposPartidosComponent implements OnInit {
  title:any = "Asignacion de Equipos a Partidos"
  Table:any
  selectedData:any=[]
  selectedMyData:any
  droppedItemsId:any=[]
  childs:any[]
  childsId:any=[]
  droppedItems:any=[]
  parentCombo:any
  typeCombo:any
  comboThirthParent:any
  selectedParent:any
  selectedType:any
  firstHora:any = "20:00:00"
  beginDate:any
  public rowsOnPage = 5;
  public search:any
  public search1:any
  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private mainService: EquiposPartidosService,
    private ChildsService: EquiposService,
    private typeService: TipoPartidosService,
    private thirthParentService: GruposService,
    private ParentsService: TorneosService
  ) { }
  ngOnInit() {
    this.cargarAll()
    // this.cargarFree()
      // this.cargarThirthParentCombo()
    this.ParentsService.getAll()
                      .then(response => {
                        this.parentCombo = response
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
    this.typeService.getAll()
                      .then(response => {
                        this.typeCombo = response
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
    let date = new Date();
    let month = date.getMonth()+1;
    let month2;
    let day = date.getDate()+1;
    let day2;
    if(month<10){
      month2='0'+month;
    }else{
      month2=month
    }
    if(day<10){
      day2='0'+day;
    }else{
      day2=day
    }
    this.beginDate= date.getFullYear()+'-'+month2+'-'+day2
  }
  onItemDrop(e: any) {
      // Get the dropped data here
      if(!this.selectedParent){
        this.createError("Debe seleccionar un Torneo")
      }else{
        let existe=(this.selectedData?this.selectedData.find(dat=>{
          return dat.id==e.dragData.id
        }):0)
        if(!existe){
          if(this.selectedData){
            if(this.selectedData.length<2){
              this.droppedItemsId.push({"id":e.dragData.id});
              this.selectedData.push(e.dragData);
              this.childsId.splice(this.childsId.findIndex(dat=>{
                return dat.id==e.dragData.id
              }),1)
              this.childs.splice(this.childs.findIndex(dat=>{
                return dat.id==e.dragData.id
              }),1)
            }
          }
        }

      }
  }

  onItemRemove(e: any) {
      // Get the dropped data here
      let existe=(this.childs.find(dat=>{
        return dat.id==e.dragData.id
      }))
      if(!existe){
        this.childsId.push({"id":e.dragData.id});
        this.childs.push(e.dragData);
        this.selectedData.splice(this.selectedData.findIndex(dat=>{
          return dat.id==e.dragData.id
        }),1)
        this.droppedItemsId.splice(this.droppedItemsId.findIndex(dat=>{
          return dat.id==e.dragData.id
        }),1)
      }



  }
  limpiar(){
    if(this.selectedData){
      this.selectedData.length=0
      this.droppedItemsId.length=0
      this.droppedItems.length=0
      this.selectedParent=null
    }
  }
  cargarThirthParentCombo(id:number){
    this.thirthParentService.getAllMineParent(id)
                      .then(response => {
                        this.comboThirthParent = response
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }

  cargarFree(id:number){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.ChildsService.getAllTipo(id)
                      .then(response => {
                        this.childs = response
                        this.childs.forEach((item,index)=>{
                          this.childsId.push({"id":item.id});
                        })
                        $('#Loading').css('display','none')
                        console.clear
                        console.log(response);

                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })
  }
  cargarAll(){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.getAll()
                      .then(response => {
                        this.Table = response
                        $("#editModal .close").click();
                        $("#insertModal .close").click();
                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })
  }
  cargarSingle(id:number){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    // this.selectedParent=id
    // if(this.droppedItemsId.length){
    //   this.droppedItemsId.length = 0;
    // }

    this.mainService.getSingle(id)
                      .then(response => {
                        this.cargarFree(response.torneos.tipo)
                        this.cargarThirthParentCombo(response.torneos.tipo)
                        this.limpiar()
                        this.selectedMyData = response
                        this.selectedParent = this.selectedMyData.torneo
                        this.selectedMyData.grupo = this.selectedMyData.equipos[0]?this.selectedMyData.equipos[0].grupo:'';
                        this.selectedMyData.equipos.forEach((item,index)=>{
                          this.droppedItemsId.push({"id":item.equipos.id});
                          this.selectedData.push(item.equipos);
                          this.childsId.splice(this.childsId.findIndex(dat=>{
                            return dat.id==item.equipos.id
                          }),1)
                          this.childs.splice(this.childs.findIndex(dat=>{
                            return dat.id==item.equipos.id
                          }),1)
                        })
                        $('#Loading').css('display','none')
                        console.clear

                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })
  }
  update(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    formValue.equipos = this.droppedItemsId
    console.log(formValue)
    this.mainService.update(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Partido Actualizado exitosamente')
                        $("#editModal .close").click();
                        $("#insertModal .close").click();
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })

  }
  delete2(id:string){
    this.mainService.delete(id)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Torneo Desasignados')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })

  }
  delete(formValue){
    this.mainService.deleteAll(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Torneo Desasignados')
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })

  }
  insert(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    formValue = {
      "torneo": this.selectedParent,
      "tipo": this.selectedType,
      "fecha": this.beginDate,
      "hora": this.firstHora,
      "equipos": this.droppedItemsId
    }

    let formValueDel = {
      "liga":this.selectedParent,
      "torneos": this.childsId
    }

  if(this.selectedParent){
    this.mainService.create(formValue)
                      .then(response => {
                        this.create('Torneo Asignados')
                        $("#editModal .close").click();
                        $("#insertModal .close").click();
                        $('#Loading').css('display','none')
                        this.cargarAll()
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })

                    }else{
                      this.createError("Debe seleccionar un Torneo")
                    }
    // this.delete(formValueDel)
  }

  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

  create(success) {
              this._service.success('¡Éxito!',success)

  }
  createError(error) {
              this._service.error('¡Error!',error)

  }
}
