import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { DataTableModule } from "angular2-datatable";

import { AdminRoutingModule } from './admin.routing';

import { SimpleNotificationsModule } from 'angular2-notifications';
import { ChartsModule } from 'ng2-charts';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2DragDropModule } from 'ng2-drag-drop';
import { LoadersCssModule } from 'angular2-loaders-css';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';

import { UsuariosService } from "./_services/usuarios.service";
import { UsersTypesService } from "./_services/users-types.service";
import { ModulosService } from "./_services/modulos.service";
import { AccesosService } from "./_services/accesos.service";
import { TipoTorneosService } from "./_services/tipo-torneos.service";
import { TipoJugadoresService } from "./_services/tipo-jugadores.service";
import { TipoEquiposService } from "./_services/tipo-equipos.service";
import { TipoPartidosService } from "./_services/tipo-partidos.service";
import { ArbitrosService } from "./_services/arbitros.service";
import { EquiposService } from "./_services/equipos.service";
import { JugadoresService } from "./_services/jugadores.service";
import { LigasService } from "./_services/ligas.service";
import { TorneosService } from "./_services/torneos.service";
import { TorneosLigasService } from "./_services/_asignaciones/torneos-ligas.service";
import { PartidosService } from "./_services/_asignaciones/partidos.service";
import { EquiposPartidosService } from "./_services/_asignaciones/equipos-partidos.service";
import { JugadoresEquiposService } from "./_services/_asignaciones/jugadores-equipos.service";
import { ArbitrajeService } from "./_services/_asignaciones/arbitraje.service";
import { GruposService } from "./_services/grupos.service";


import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuariosTipoComponent } from './usuarios-tipo/usuarios-tipo.component';
import { LoaderComponent } from './loader/loader.component';
import { ProfileComponent } from './profile/profile.component';
import { TipoJugadoresComponent } from './tipos/tipo-jugadores/tipo-jugadores.component';
import { TipoEquiposComponent } from './tipos/tipo-equipos/tipo-equipos.component';
import { TipoPartidosComponent } from './tipos/tipo-partidos/tipo-partidos.component';
import { TipoTorneosComponent } from './tipos/tipo-torneos/tipo-torneos.component';
import { JugadoresComponent } from './jugadores/jugadores.component';
import { EquiposComponent } from './equipos/equipos.component';
import { TorneosComponent } from './torneos/torneos.component';
import { LigasComponent } from './ligas/ligas.component';
import { ArbitrosComponent } from './arbitros/arbitros.component';
import { TorneosLigasComponent } from './torneos-ligas/torneos-ligas.component';
import { PartidosComponent } from './partidos/partidos.component';
import { EquiposPartidosComponent } from './equipos-partidos/equipos-partidos.component';
import { JugadoresEquiposComponent } from './jugadores-equipos/jugadores-equipos.component';
import { ArbitrajeComponent } from './arbitraje/arbitraje.component';
import { GruposComponent } from './grupos/grupos.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ChartsModule,
    SimpleNotificationsModule.forRoot(),
    Ng2SearchPipeModule,
    AngularMultiSelectModule,
    Ng2DragDropModule.forRoot(),
    LoadersCssModule,
    AdminRoutingModule
  ],
  declarations: [
    AdminComponent,
    DashboardComponent,
    UsuariosComponent,
    LoaderComponent,
    UsuariosTipoComponent,
    ProfileComponent,
    TipoJugadoresComponent,
    TipoEquiposComponent,
    TipoPartidosComponent,
    TipoTorneosComponent,
    JugadoresComponent,
    EquiposComponent,
    TorneosComponent,
    LigasComponent,
    ArbitrosComponent,
    TorneosLigasComponent,
    PartidosComponent,
    EquiposPartidosComponent,
    JugadoresEquiposComponent,
    ArbitrajeComponent,
    GruposComponent
  ],
  providers: [
    UsuariosService,
    UsersTypesService,
    ModulosService,
    AccesosService,
    TipoTorneosService,
    TipoJugadoresService,
    TipoEquiposService,
    TipoPartidosService,
    ArbitrosService,
    EquiposService,
    JugadoresService,
    LigasService,
    TorneosService,
    TorneosLigasService,
    PartidosService,
    EquiposPartidosService,
    JugadoresEquiposService,
    ArbitrajeService,
    GruposService
  ]
})
export class AdminModule { }
