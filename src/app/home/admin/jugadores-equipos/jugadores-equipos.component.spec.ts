import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JugadoresEquiposComponent } from './jugadores-equipos.component';

describe('JugadoresEquiposComponent', () => {
  let component: JugadoresEquiposComponent;
  let fixture: ComponentFixture<JugadoresEquiposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JugadoresEquiposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JugadoresEquiposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
