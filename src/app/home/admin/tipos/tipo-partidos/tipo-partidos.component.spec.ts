import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoPartidosComponent } from './tipo-partidos.component';

describe('TipoPartidosComponent', () => {
  let component: TipoPartidosComponent;
  let fixture: ComponentFixture<TipoPartidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoPartidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoPartidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
