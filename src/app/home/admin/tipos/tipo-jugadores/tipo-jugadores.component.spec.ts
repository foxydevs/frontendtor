import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoJugadoresComponent } from './tipo-jugadores.component';

describe('TipoJugadoresComponent', () => {
  let component: TipoJugadoresComponent;
  let fixture: ComponentFixture<TipoJugadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoJugadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoJugadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
