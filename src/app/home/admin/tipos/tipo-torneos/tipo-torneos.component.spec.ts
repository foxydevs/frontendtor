import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoTorneosComponent } from './tipo-torneos.component';

describe('TipoTorneosComponent', () => {
  let component: TipoTorneosComponent;
  let fixture: ComponentFixture<TipoTorneosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoTorneosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoTorneosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
