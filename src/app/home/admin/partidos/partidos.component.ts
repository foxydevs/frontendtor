import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { PartidosService } from "./../_services/_asignaciones/partidos.service";
import { TipoPartidosService } from "./../_services/tipo-partidos.service";
import { GruposService } from "./../_services/grupos.service";
import { TorneosService } from "./../_services/torneos.service";
import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  selector: 'app-partidos',
  templateUrl: './partidos.component.html',
  styleUrls: ['./partidos.component.css']
})
export class PartidosComponent implements OnInit {
  title:any = "Partidos"
  Table:any
  comboParent:any
  comboSecondParent:any
  comboThirthParent:any
  selectedData:any
  firstHora:any = "20:00:00"
  beginDate:any
  diasConst:any = 7
  tipoConst:any = 2
  periodoConst:any = 1
  public rowsOnPage = 5;
  public search:any
  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private mainService: PartidosService,
    private secondParentService: TorneosService,
    private thirthParentService: GruposService,
    private parentService: TipoPartidosService
  ) { }

    ngOnInit() {

      this.iniciar()
    }
    iniciar(){
      this.cargarAll()
      this.cargarParentCombo()
      this.cargarSecondParentCombo()
      // this.cargarThirthParentCombo()
      let date = new Date();
      let month = date.getMonth()+1;
      let month2;
      let day = date.getDate();
      let day2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month
      }
      if(day<10){
        day2='0'+day;
      }else{
        day2=day
      }
      this.beginDate= date.getFullYear()+'-'+month2+'-'+day2;
      let hour = date.getHours();
      let hour2;
      let minute = date.getMinutes();
      let minute2;
      let second = date.getSeconds();
      let second2;
      if(hour<10){
        hour2='0'+hour;
      }else{
        hour2=hour
      }
      if(minute<10){
        minute2='0'+minute;
      }else{
        minute2=minute
      }
      if(second<10){
        second2='0'+second;
      }else{
        second2=second
      }
      this.firstHora= hour2+':'+minute2+':'+second2;
      let data = {
        tipoAdd : 1,
        periodoAdd : 1,
        cantidadAdd : 1
      }
      this.aumentarDate(month+'-'+day+'-'+date.getFullYear()+' '+hour2+':'+minute2+':'+second2,data)
    }
    aumentarDate(fecha:string,request:any){
      let date = new Date(fecha);
      // console.log(fecha+' == '+this.firstHora);
      request.tipoAdd = +request.tipoAdd;
      request.periodoAdd = +request.periodoAdd;
      switch(request.tipoAdd){
        case 1:{
          switch(request.periodoAdd){
            case 1:{
              date.setHours(date.getHours() + request.cantidadAdd)
              // console.log('agregado '+request.cantidadAdd+' hora');
              break;
            }
            case 2:{
              date.setMinutes(date.getMinutes() + request.cantidadAdd)
              // console.log('agregado '+request.cantidadAdd+' minuto');
              break;
            }
            case 3:{
              date.setSeconds(date.getSeconds() + request.cantidadAdd)
              // console.log('agregado '+request.cantidadAdd+' segundos');
              break;
            }
          }
          break;
        }
        case 2:{
          switch(request.periodoAdd){
            case 1:{
              date.setDate(date.getDate() + request.cantidadAdd)
              // console.log('agregado '+request.cantidadAdd+' dia');
              break;
            }
            case 2:{
              date.setMonth((date.getMonth()) + request.cantidadAdd)
              // console.log('agregado '+request.cantidadAdd+' mes');

              break;
            }
            case 3:{
              date.setFullYear(date.getFullYear() + request.cantidadAdd)
              // console.log('agregado '+request.cantidadAdd+' anio');
              break;
            }
          }
          break;
        }
      }
      let month = date.getMonth()+1;
      let month2;
      let day = date.getDate();
      let day2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month
      }
      if(day<10){
        day2='0'+day;
      }else{
        day2=day
      }
      this.beginDate= date.getFullYear()+'-'+month2+'-'+day2
      let hour = date.getHours();
      let hour2;
      let minute = date.getMinutes();
      let minute2;
      let second = date.getSeconds();
      let second2;
      if(hour<10){
        hour2='0'+hour;
      }else{
        hour2=hour
      }
      if(minute<10){
        minute2='0'+minute;
      }else{
        minute2=minute
      }
      if(second<10){
        second2='0'+second;
      }else{
        second2=second
      }
      this.firstHora= hour2+':'+minute2+':'+second2;
      // console.log(this.beginDate+' == '+this.firstHora);

    }
    cargarAll(){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.getAll()
                        .then(response => {
                          this.Table = response

                          $('#Loading').css('display','none')
                          console.clear
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    cargarParentCombo(){
      this.parentService.getAll()
                        .then(response => {
                          this.comboParent = response
                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    cargarSecondParentCombo(){
      this.secondParentService.getAll()
                        .then(response => {
                          this.comboSecondParent = response
                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    cargarThirthParentCombo(id:number){
      this.thirthParentService.getAllMineParent(id)
                        .then(response => {
                          this.comboThirthParent = response
                          // console.log(response);

                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    cargarSingle(id:number){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.getSingle(id)
                        .then(response => {
                          this.selectedData = response;
                          this.cargarThirthParentCombo(response.torneo);
                          this.iniciar();
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    update(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      // console.log(data)
      this.mainService.update(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Partido Actualizado exitosamente')
                          $('#Loading').css('display','none')
                          $("#editModal .close").click();
                          console.log(response);

                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

    }
    delete(id:string){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      if(confirm("¿Desea eliminar el Partido?")){
      this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Partido Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
      }else{
        $('#Loading').css('display','none')
      }

    }
    addDate(formValue:any){

      let fech=formValue.fecha.split('-')
      this.aumentarDate(fech[1]+'-'+fech[2]+'-'+fech[0]+' '+formValue.hora,formValue)

      // $('#Loading').css('display','block')
      // $('#Loading').addClass('in')
      // this.mainService.create(formValue)
      //                   .then(response => {
      //                     this.cargarAll()
      //                     console.clear
      //                     this.create('Partido Ingresado')
      //                     let fech=formValue.fecha.split('-')
      //                     this.aumentarDate(fech[1]+'-'+fech[2]+'-'+fech[0],formValue)
      //                     $('#Loading').css('display','none')
      //                   }).catch(error => {
      //                     console.clear
      //                     $('#Loading').css('display','none')
      //                     this.createError(error)
      //                   })

    }
    insert(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.create(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Partido Ingresado')
                          $('#Loading').css('display','none')

                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }

  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }
}
