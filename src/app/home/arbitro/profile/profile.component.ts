import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { UsuariosService } from "./../../admin/_services/usuarios.service";
import { NotificationsService } from 'angular2-notifications';

declare var $: any
import { path } from "../../../config.module";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  userTable:any
  userTypesCombo:any
  foreignCombo:any
  foreignData:any
  selectedUser:any
  selectedUserId:any = localStorage.getItem('currentId');
  public rowsOnPage = 5;
  public search:any
  Data:any
  private basePath:string = path.path

  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UsuariosService
  ) { }

  subirImagenes(archivo,form,id){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    var archivos=archivo.srcElement.files;
    let url = `${this.basePath}/api/usuarios/${form.id}/upload`

    var i=0;
    var size=archivos[i].size;
    var type=archivos[i].type;
        if(size<(2*(1024*1024))){
          if(type=="image/png" || type=="image/jpeg" || type=="image/jpg"){
        $("#"+id).upload(url,
            {
              avatar: archivos[i]
          },
          function(respuesta)
          {
            $('#imgAvatar').attr("src",'')
            $('#imgAvatar').attr("src",respuesta.picture)
            $('#Loading').css('display','none')
            $("#"+id).val('')
            localStorage.setItem('currentPicture', respuesta.picture);
            $("#barra_de_progreso").val(0)
          },
          function(progreso, valor)
          {

            $("#barra_de_progreso").val(valor);
          }
        );
          }else{
            this.createError("El tipo de imagen no es valido")
            $('#Loading').css('display','none')
          }
      }else{
        this.createError("La imagen es demaciado grande")
        $('#Loading').css('display','none')
      }
  }
    ngOnInit() {
      this.userService.getTypes()
                        .then(response => {
                          this.userTypesCombo = response
                          this.cargarUser(this.selectedUserId)
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    cargarUsers(){
      this.userService.getAll()
                        .then(response => {
                          this.userTable = response
                          $("#editModal .close").click();
                          $("#insertModal .close").click();
                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    cargarUser(id:number){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.userService.getSingle(id)
                        .then(response => {
                          this.selectedUser = response;
                          // console.log(response);
                          this.cargarForanea(response.tipo+'');
                          switch (response.tipo+'') {
                            case '1':{
                              this.selectedUser.foreign=response.liga
                              break;
                          }
                            case '2':{
                              this.selectedUser.foreign=response.equipo
                              break;
                          }
                            case '3':{
                              this.selectedUser.foreign=response.arbitro
                              break;
                          }
                            default:{
                              break;}
                          }
                          if(this.selectedUser.foreign){
                            this.getForeign(this.selectedUser.foreign+'',response.tipo+'')
                          }
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    updateUser(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      let arbitro:any = null
      let equipo:any = null
      let liga:any = null
      switch (formValue.tipo) {
        case '1':{
          liga=this.selectedUser.foreign*1
          break;
        }
        case '2':{
          equipo=this.selectedUser.foreign*1
          break;
        }
        case '3':{
          arbitro=this.selectedUser.foreign*1
          break;
        }
        case 1:{
          liga=this.selectedUser.foreign*1
          break;
        }
        case 2:{
          equipo=this.selectedUser.foreign*1
          break;
        }
        case 3:{
          arbitro=this.selectedUser.foreign*1
          break;
        }
        default:{
          break;
        }
      }


      let data = {
        id: formValue.id,
        username: formValue.username,
        email: formValue.email,
        tipo: formValue.tipo,
        liga: liga,
        equipo: equipo,
        arbitro: arbitro
      }
      //console.log(data)
      this.userService.update(data)
                        .then(response => {
                          // console.log(data);
                          this.cargarUsers()
                          console.clear
                          this.create('Usuario Actualizado exitosamente')
                          $('#Loading').css('display','none')

                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

    }
    updatePass(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      let data = {
        id: this.selectedUserId,
        old_pass: formValue.old_pass,
        new_pass: formValue.new_pass,
        new_pass_rep: formValue.new_pass2
      }
      //console.log(data)
      this.userService.updatePass(data)
                        .then(response => {
                          console.clear
                          this.create('Usuario Actualizado exitosamente')
                          $('#Loading').css('display','none')
                          $('#passChange-form')[0].reset()
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    generar(longitud)
    {
      let i:number
      var caracteres = "123456789+/-*abcdefghijkmnpqrtuvwxyz123456789+/-*ABCDEFGHIJKLMNPQRTUVWXYZ12346789+/-*";
      var contraseña = "";
      for (i=0; i<longitud; i++) contraseña += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
      return contraseña;
    }
    comboTutor(id:string){
      this.userService.getArbitro(id)
                        .then(response => {
                          this.Data = {
                            lastnameData:response.lastname,
                            firstnameData:response.firstname
                          }
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    comboTeacher(id:string){
      this.userService.getEquipo(id)
                        .then(response => {
                          this.Data = {
                            lastnameData:response.lastname,
                            firstnameData:response.firstname
                          }
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    comboStudent(id:string){
      this.userService.getLiga(id)
                        .then(response => {
                          this.Data = {
                            lastnameData:response.lastname,
                            firstnameData:response.firstname
                          }
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    getForeign(id:string,type:string){
      //console.log(`${id} ${type}`)
      switch (type) {
        case '1':{
          this.comboStudent(id)
          break;
        }
        case '2':{
          this.comboTeacher(id)
          break;
        }
        case '3':{
          this.comboTutor(id)
          break;
        }
        default:{
          console.log(`${id} id
          ${type} tipo`)
          break;
        }
      }
    }
    comboTutors(type:string){
      this.userService.getArbitros()
                        .then(response => {
                          this.foreignCombo = response
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    comboTeachers(type:string){
      this.userService.getEquipos()
                        .then(response => {
                          this.foreignCombo = response
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    comboStudents(type:string){
      this.userService.getLigas()
                        .then(response => {
                          this.foreignCombo = response
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
    cargarForanea(type:string){

      switch (type) {
        case '1':{
          this.comboStudents(type)
          this.foreignData = {
            title:'Ligas',
            type: type
          };
          break;
        }
        case '2':{
          this.comboTeachers(type)
          this.foreignData = {
            title:'Equipos',
            type: type
          };
          break;
        }
        case '3':{
          this.comboTutors(type)
          this.foreignData = {
            title:'Arbitros',
            type: type
          };
          break;
        }
        case '4':{
          this.foreignData = {
            title:'',
            type: type
          };
          break;
        }
        default:{
          this.foreignData = {
            title:'',
            type: type
          };
          console.log(`combo foraneo no encontrado ${type} tipo`)
          break;
        }
      }
    }
  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }
}
