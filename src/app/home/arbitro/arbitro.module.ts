import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { DataTableModule } from "angular2-datatable";

import { ArbitroRoutingModule } from './arbitro.routing';

import { SimpleNotificationsModule } from 'angular2-notifications';
import { ChartsModule } from 'ng2-charts';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2DragDropModule } from 'ng2-drag-drop';
import { LoadersCssModule } from 'angular2-loaders-css';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';

import { TipoTorneosService } from "./../admin/_services/tipo-torneos.service";
import { TipoJugadoresService } from "./../admin/_services/tipo-jugadores.service";
import { TipoEquiposService } from "./../admin/_services/tipo-equipos.service";
import { TipoPartidosService } from "./../admin/_services/tipo-partidos.service";
import { ArbitrosService } from "./../admin/_services/arbitros.service";
import { EquiposService } from "./../admin/_services/equipos.service";
import { JugadoresService } from "./../admin/_services/jugadores.service";
import { TorneosService } from "./../admin/_services/torneos.service";
import { PartidosService } from "./../admin/_services/_asignaciones/partidos.service";
import { EquiposPartidosService } from "./../admin/_services/_asignaciones/equipos-partidos.service";
import { JugadoresEquiposService } from "./../admin/_services/_asignaciones/jugadores-equipos.service";
import { ArbitrajeService } from "./../admin/_services/_asignaciones/arbitraje.service";
import { LigasService } from "./../admin/_services/ligas.service";

import { GolesService } from "./_services/goles.service";
import { TarjetasService } from "./_services/tarjetas.service";

import { ArbitroComponent } from './arbitro.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoaderComponent } from './loader/loader.component';
import { ProfileComponent } from './profile/profile.component';
import { GolesComponent } from './goles/goles.component';
import { TarjetasComponent } from './tarjetas/tarjetas.component';
import { ArbitrajeComponent } from './arbitraje/arbitraje.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ChartsModule,
    SimpleNotificationsModule.forRoot(),
    Ng2SearchPipeModule,
    AngularMultiSelectModule,
    Ng2DragDropModule.forRoot(),
    LoadersCssModule,
    ArbitroRoutingModule
  ],
  declarations: [
    ArbitroComponent,
    DashboardComponent,
    LoaderComponent,
    ProfileComponent,
    ArbitrajeComponent,
    GolesComponent,
    TarjetasComponent,
  ],
  providers: [
    ArbitrosService,
    TipoTorneosService,
    TipoJugadoresService,
    TipoEquiposService,
    TipoPartidosService,
    EquiposService,
    JugadoresService,
    TorneosService,
    PartidosService,
    EquiposPartidosService,
    JugadoresEquiposService,
    ArbitrajeService,
    LigasService,
    GolesService,
    TarjetasService
  ]
})
export class ArbitroModule { }
