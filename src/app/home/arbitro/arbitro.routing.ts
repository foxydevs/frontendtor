import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArbitroComponent } from "./arbitro.component";
import { DashboardComponent } from "./dashboard/dashboard.component";

import { GolesComponent } from "./goles/goles.component";
import { TarjetasComponent } from "./tarjetas/tarjetas.component";
import { ArbitrajeComponent } from "./arbitraje/arbitraje.component";

import { ProfileComponent } from "./profile/profile.component";

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: ArbitroComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'goles', component: GolesComponent },
    { path: 'tarjetas', component: TarjetasComponent },
    { path: 'arbitraje', component: ArbitrajeComponent },
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArbitroRoutingModule { }
