import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbitrajeComponent } from './arbitraje.component';

describe('ArbitrajeComponent', () => {
  let component: ArbitrajeComponent;
  let fixture: ComponentFixture<ArbitrajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArbitrajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbitrajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
