import { TestBed, inject } from '@angular/core/testing';

import { GolesService } from './goles.service';

describe('GolesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GolesService]
    });
  });

  it('should be created', inject([GolesService], (service: GolesService) => {
    expect(service).toBeTruthy();
  }));
});
