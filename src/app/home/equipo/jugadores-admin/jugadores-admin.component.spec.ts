import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JugadoresAdminComponent } from './jugadores-admin.component';

describe('JugadoresAdminComponent', () => {
  let component: JugadoresAdminComponent;
  let fixture: ComponentFixture<JugadoresAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JugadoresAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JugadoresAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
