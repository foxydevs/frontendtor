import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { PartidosService } from "./../../admin/_services/_asignaciones/partidos.service";
import { TipoPartidosService } from "./../../admin/_services/tipo-partidos.service";
import { TorneosService } from "./../../admin/_services/torneos.service";
import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  selector: 'app-partidos',
  templateUrl: './partidos.component.html',
  styleUrls: ['./partidos.component.css']
})
export class PartidosComponent implements OnInit {
  title:any = "Partidos"
  Table:any
  comboParent:any
  comboSecondParent:any
  selectedData:any
  firstHora:any = "20:00:00"
  beginDate:any
  diasConst:any = 7
  public rowsOnPage = 5;
  public search:any
  id=localStorage.getItem('currentLocalId');
  constructor(
    private _service: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private mainService: PartidosService,
    private secondParentService: TorneosService,
    private parentService: TipoPartidosService
  ) { }

    ngOnInit() {

      this.cargarAll()
      let date = new Date();
      let month = date.getMonth()+1;
      let month2;
      let day = date.getDate();
      let day2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month
      }
      if(day<10){
        day2='0'+day;
      }else{
        day2=day
      }
      this.beginDate= date.getFullYear()+'-'+month2+'-'+day2
      this.aumentarDate(month+'/'+day+'/'+date.getFullYear(),1)
    }
    aumentarDate(fecha:string,cant:number){
      let date = new Date(fecha);
      date.setDate(date.getDate() + cant)
      let month = date.getMonth()+1;
      let month2;
      let day = date.getDate();
      let day2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month
      }
      if(day<10){
        day2='0'+day;
      }else{
        day2=day
      }
      this.beginDate= date.getFullYear()+'-'+month2+'-'+day2
    }
    cargarAll(){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.getAllMineEquipo(+this.id)
                        .then(response => {
                          this.Table = response
                          $("#editModal .close").click();
                          $("#insertModal .close").click();
                          $('#Loading').css('display','none')
                          console.clear
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }

    cargarSingle(id:number){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.getSingle(id)
                        .then(response => {
                          this.selectedData = response;
                          console.log(response);

                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    update(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      //console.log(data)
      this.mainService.update(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Partido Actualizado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

    }
    delete(id:string){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      if(confirm("¿Desea eliminar el Partido?")){
      this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Partido Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
      }else{
        $('#Loading').css('display','none')
      }

    }
    addDate(formValue:any){


      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.create(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Partido Ingresado')
                          let fech=formValue.fecha.split('-')
                          this.aumentarDate(fech[1]+'-'+fech[2]+'-'+fech[0],formValue.cant)
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })

    }
    insert(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.create(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Partido Ingresado')
                          $('#Loading').css('display','none')
                          $('#insert-form')[0].reset()
                          $("#insertAutoModal .close").click();

                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }

  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }
}
