import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EquipoComponent } from "./equipo.component";
import { DashboardComponent } from "./dashboard/dashboard.component";

import { ProfileComponent } from "./profile/profile.component";
import { PartidosComponent } from "./partidos/partidos.component";
import { CalendarioComponent } from "./calendario/calendario.component";
import { JugadoresComponent } from "./jugadores/jugadores.component";
import { JugadoresAdminComponent } from "./jugadores-admin/jugadores-admin.component";

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: EquipoComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'partidos', component: PartidosComponent },
    { path: 'calendario', component: CalendarioComponent },
    { path: 'jugadores', component: JugadoresComponent },
    { path: 'jugadores-admin', component: JugadoresAdminComponent },
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EquipoRoutingModule { }
