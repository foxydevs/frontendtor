import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavComponent } from "./nav.component";
import { LigaGuard } from "./../_guards/liga.guard";
import { AdminGuard } from "./../_guards/admin.guard";
import { ArbitroGuard } from "./../_guards/arbitro.guard";
import { EquipoGuard } from "./../_guards/equipo.guard";

const routes: Routes = [
  { path: '', redirectTo: 'usuario', pathMatch: 'full' },
  { path: '', component: NavComponent, children: [
    { path: 'admin',loadChildren: 'app/home/admin/admin.module#AdminModule', canActivate: [AdminGuard]},
    { path: 'liga',loadChildren: 'app/home/liga/liga.module#LigaModule', canActivate: [LigaGuard]},
    { path: 'arbitro',loadChildren: 'app/home/arbitro/arbitro.module#ArbitroModule', canActivate: [ArbitroGuard]},
    { path: 'equipo',loadChildren: 'app/home/equipo/equipo.module#EquipoModule', canActivate: [EquipoGuard]},
  ]},
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
