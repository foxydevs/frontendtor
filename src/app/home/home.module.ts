import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { LoadersCssModule } from 'angular2-loaders-css';

import { HomeRoutingModule } from './home.routing';
import { NavComponent } from './nav.component';

import { AdminGuard } from "./../_guards/admin.guard";
import { LigaGuard } from "./../_guards/liga.guard";
import { ArbitroGuard } from "./../_guards/arbitro.guard";
import { EquipoGuard } from "./../_guards/equipo.guard";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    LoadersCssModule,
    HomeRoutingModule
  ],
  declarations: [NavComponent],
  providers: [
    AdminGuard,
    LigaGuard,
    ArbitroGuard,
    EquipoGuard
  ]
})
export class HomeModule { }
