import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { DataTableModule } from "angular2-datatable";

import { PublicRoutingModule } from './public.routing'

import { SimpleNotificationsModule } from 'angular2-notifications';
import { ChartsModule } from 'ng2-charts';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2DragDropModule } from 'ng2-drag-drop';
import { LoadersCssModule } from 'angular2-loaders-css';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { CalendarModule } from 'ap-angular2-fullcalendar';

import { TipoTorneosService } from "./../home/admin/_services/tipo-torneos.service";
import { TipoJugadoresService } from "./../home/admin/_services/tipo-jugadores.service";
import { TipoEquiposService } from "./../home/admin/_services/tipo-equipos.service";
import { TipoPartidosService } from "./../home/admin/_services/tipo-partidos.service";
import { ArbitrosService } from "./../home/admin/_services/arbitros.service";
import { EquiposService } from "./../home/admin/_services/equipos.service";
import { JugadoresService } from "./../home/admin/_services/jugadores.service";
import { TorneosService } from "./../home/admin/_services/torneos.service";
import { LigasService } from "./../home/admin/_services/ligas.service";
import { PartidosService } from "./../home/admin/_services/_asignaciones/partidos.service";
import { EquiposPartidosService } from "./../home/admin/_services/_asignaciones/equipos-partidos.service";
import { JugadoresEquiposService } from "./../home/admin/_services/_asignaciones/jugadores-equipos.service";
import { ArbitrajeService } from "./../home/admin/_services/_asignaciones/arbitraje.service";
import { GruposService } from '../home/admin/_services/grupos.service';

import { PublicComponent } from './public.component';
import { InicioComponent } from './inicio/inicio.component';
import { CalendarioComponent } from './calendario/calendario.component';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ChartsModule,
    SimpleNotificationsModule.forRoot(),
    Ng2SearchPipeModule,
    AngularMultiSelectModule,
    Ng2DragDropModule.forRoot(),
    LoadersCssModule,
    CalendarModule,
    PublicRoutingModule
  ],
  declarations: [
    PublicComponent,
    InicioComponent,
    LoaderComponent,
    CalendarioComponent
  ],
  providers: [
    ArbitrosService,
    TipoTorneosService,
    TipoJugadoresService,
    TipoEquiposService,
    TipoPartidosService,
    EquiposService,
    JugadoresService,
    TorneosService,
    PartidosService,
    EquiposPartidosService,
    JugadoresEquiposService,
    ArbitrajeService,
    GruposService,
    LigasService
  ]
})
export class PublicModule { }
