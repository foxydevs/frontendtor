import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { PartidosService } from "./../../home/admin/_services/_asignaciones/partidos.service";

import { NotificationsService } from 'angular2-notifications';
import * as $ from 'jquery';
@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.css']
})
export class CalendarioComponent implements OnInit
{
  calendarOptions:Object
  Eventos:any = []
  title:String = "Calendario de Partidos"
  id=localStorage.getItem('currentLocalId');
  idLiga = localStorage.getItem('currentLiga');
  view:string = "month"
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location,
    private _service: NotificationsService,
    private mainService: PartidosService
  ) { }

  ngOnInit() {
    this.idLiga = localStorage.getItem('currentLiga');
    console.log(this.idLiga);
    this.cargarEventos();
    let date = new Date();
    this.calendarOptions = {
      fixedWeekCount : true,
      height:600,
      lang: 'es',
      defaultDate: date.getFullYear()+'-'+(((date.getMonth()+1)<10)?'0'+(date.getMonth()+1):(date.getMonth()+1))+'-'+(((date.getDate())<10)?'0'+(date.getDate()):(date.getDate())),
      editable: false,
      titleFormat: 'MMMM YYYY',
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles',
      'Jueves', 'Viernes', 'Sabado'],
      dayNamesShort:['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
      'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
      'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
      firstDay: 0,
      weekends: true,
      timeFormat: 'h:mm t\m',
      header: {
        left: 'month,agendaWeek,agendaDay', // buttons for switching between views
        center: 'title'
      },
      eventLimit: true, // allow "more" link when too many events
      events: this.Eventos
    };
  }
  goToBack() {
    this.location.back();
  }
  cargarEventos(){
    this.mainService.getAllMine(+this.id)
                        .then(response => {
                          console.log(response);

                          response.forEach(element => {
                          console.log(element.fecha+'T'+element.hora);
                            this.Eventos.push(
                              {
                                title: '\n'+(element.equipos[0]?element.equipos[0].equipos.nombre:'Por Definir')+' vrs '+(element.equipos[1]?element.equipos[1].equipos.nombre:'Por Definir')+'\n'+(element.equipos[0]?element.equipos[0].resultado:'0')+' - '+(element.equipos[1]?element.equipos[1].resultado:'0'),
                                start: element.fecha+'T'+element.hora,
                                allDay: false
                              }
                            )

                          });

                          console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })

  }

    public options = {
      position: ["bottom", "right"],
      timeOut: 2000,
      lastOnBottom: false,
      animate: "fromLeft",
      showProgressBar: false,
      pauseOnHover: true,
      clickToClose: true,
      maxLength: 200
  };

  create(success) {
      this._service.success('¡Éxito!',success)

  }
  createError(error) {
      this._service.error('¡Error!',error)

  }
}
